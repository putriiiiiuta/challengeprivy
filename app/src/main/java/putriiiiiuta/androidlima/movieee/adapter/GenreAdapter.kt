package putriiiiiuta.androidlima.movieee.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import putriiiiiuta.androidlima.movieee.model.GenreModel
import putriiiiiuta.androidlima.movieee.databinding.ItemGenreBinding

class GenreAdapter : RecyclerView.Adapter<GenreAdapter.GenreAdapterVH>() {

    private val genreList = mutableListOf<GenreModel>()
    private var eventListener: EventListener? = null

    fun submitList(newList: MutableList<GenreModel>) {
        genreList.apply {
            notifyItemRangeRemoved(0, itemCount)
            clear()
            addAll(newList)
            notifyItemRangeChanged(0, itemCount)
        }
    }

    inner class GenreAdapterVH(private val binding: ItemGenreBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(genre: GenreModel) {
            with(binding) {
                tvItemGenre.text = genre.genreName
                root.setOnClickListener {
                    eventListener?.onItemClick(genre)
                }
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): GenreAdapterVH {
        val binding = ItemGenreBinding.inflate(
            LayoutInflater.from(parent.context),
            parent,
            false
        )
        return GenreAdapterVH(binding)
    }

    override fun onBindViewHolder(holder: GenreAdapterVH, position: Int) {
        holder.bind(genreList[position])
    }

    override fun getItemCount(): Int {
        return genreList.size
    }

    interface EventListener {
        fun onItemClick(genre: GenreModel)
    }

    fun setEventListener(eventListener: EventListener) {
        this.eventListener = eventListener
    }
}