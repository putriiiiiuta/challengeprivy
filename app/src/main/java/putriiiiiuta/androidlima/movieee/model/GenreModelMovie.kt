package putriiiiiuta.androidlima.movieee.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize
import putriiiiiuta.androidlima.movieee.data.remote.GenreMovieeeResponse

@Parcelize
data class MovieByGenreModel(
    val movieId: Int,
    val movieTitle: String,
    val movieImage: String,
) : Parcelable

fun GenreMovieeeResponse.toMovieByGenreList(): MutableList<MovieByGenreModel> {
    return this.results?.map { response ->
        MovieByGenreModel(
            movieId = response?.id ?: 0,
            movieTitle = response?.title.orEmpty(),
            movieImage = response?.posterPath.orEmpty()
        )
    }.orEmpty().toMutableList()
}