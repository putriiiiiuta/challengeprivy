package putriiiiiuta.androidlima.movieee.model

import android.os.Parcelable
import putriiiiiuta.androidlima.movieee.data.remote.GenreResponse
import kotlinx.parcelize.Parcelize

@Parcelize
data class GenreModel(
    val genreId: Int,
    val genreName: String
): Parcelable

fun GenreResponse.toGenreList(): MutableList<GenreModel> {
    return this.genres?.map { response ->
        GenreModel(
            genreId = response?.id ?: 0,
            genreName = response?.name.orEmpty()
        )
    }.orEmpty().toMutableList()
}