package putriiiiiuta.androidlima.movieee.mimovie.data.model

import putriiiiiuta.androidlima.movieee.data.remote.MovieDetailResponse

data class DetailMovieModel(
    val movieId: Int,
    val movieTitle: String,
    val movieImage: String,
    val movieOverview: String,
)

fun MovieDetailResponse.toDetailMovie(): DetailMovieModel {
    return DetailMovieModel(
        movieId = this.id ?: 0,
        movieTitle = this.title.orEmpty(),
        movieImage = this.posterPath.orEmpty(),
        movieOverview = this.overview.orEmpty()
    )
}