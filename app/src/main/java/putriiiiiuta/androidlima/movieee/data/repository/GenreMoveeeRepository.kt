package putriiiiiuta.androidlima.movieee.data.repository

import androidx.lifecycle.LiveData
import putriiiiiuta.androidlima.movieee.data.remote.ApiResponse
import putriiiiiuta.androidlima.movieee.data.remote.GenreMovieeeResponse
import putriiiiiuta.androidlima.movieee.data.source.DataSource

class GenreMoveeeRepository private constructor(private val dataSource: DataSource) {

    companion object {
        @Volatile
        private var instance: GenreMoveeeRepository? = null

        fun getInstance(dataSource: DataSource): GenreMoveeeRepository {
            return instance ?: synchronized(this) {
                instance ?: GenreMoveeeRepository(dataSource)
            }
        }
    }

    fun getMovieByGenre(genreId: Int): LiveData<ApiResponse<GenreMovieeeResponse>> {
        return dataSource.getMovieByGenre(genreId)
    }
}