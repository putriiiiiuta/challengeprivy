package putriiiiiuta.androidlima.movieee.data.repository

import androidx.lifecycle.LiveData
import putriiiiiuta.androidlima.movieee.data.remote.ApiResponse
import putriiiiiuta.androidlima.movieee.data.remote.GenreResponse
import putriiiiiuta.androidlima.movieee.data.source.DataSource

class GenreRepository private constructor(private val dataSource: DataSource) {

    companion object {
        @Volatile
        private var instance: GenreRepository? = null

        fun getInstance(dataSource: DataSource): GenreRepository {
            return instance ?: synchronized(this) {
                instance ?: GenreRepository(dataSource)
            }
        }
    }

    fun getAllGenre(): LiveData<ApiResponse<GenreResponse>> {
        return dataSource.getAllGenre()
    }

}