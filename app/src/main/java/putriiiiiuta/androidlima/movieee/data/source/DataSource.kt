package putriiiiiuta.androidlima.movieee.data.source

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import putriiiiiuta.androidlima.movieee.network.ApiService
import putriiiiiuta.androidlima.movieee.data.remote.ApiResponse
import putriiiiiuta.androidlima.movieee.data.remote.GenreResponse
import putriiiiiuta.androidlima.movieee.data.remote.GenreMovieeeResponse
import putriiiiiuta.androidlima.movieee.data.remote.MovieDetailResponse
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.IOException

class DataSource private constructor(private val apiService: ApiService) {
    companion object {
        private val TAG = DataSource::class.java.simpleName.toString()
        private const val ERROR_MESSAGE_IO_EXCEPTION = "Periksa koneksi internet Anda"

        @Volatile
        private var instance: DataSource? = null

        fun getInstance(apiService: ApiService): DataSource {
            return instance ?: synchronized(this) {
                instance ?: DataSource(apiService)
            }
        }
    }

    fun getAllGenre(): LiveData<ApiResponse<GenreResponse>> {
        val result = MutableLiveData<ApiResponse<GenreResponse>>().apply {
            value = ApiResponse.Loading
        }

        apiService.getAllGenre().enqueue(object : Callback<GenreResponse> {
            override fun onResponse(call: Call<GenreResponse>, response: Response<GenreResponse>) {
                val data = response.body()
                println("cek data: $data")
                result.value = if (response.isSuccessful && data != null) {
                    ApiResponse.Success(data)
                } else {
                    ApiResponse.Error(response.message())
                }
            }

            override fun onFailure(call: Call<GenreResponse>, t: Throwable) {
                val errorMessage = if (t is IOException) ERROR_MESSAGE_IO_EXCEPTION
                else t.message.toString()
                result.value = ApiResponse.Error(errorMessage)
                Log.e(TAG, "${call.request().url}\n${t.stackTraceToString()}")
            }

        })

        return result
    }

    fun getMovieByGenre(genreId: Int): LiveData<ApiResponse<GenreMovieeeResponse>> {
        val result = MutableLiveData<ApiResponse<GenreMovieeeResponse>>().apply {
            value = ApiResponse.Loading
        }

        apiService.getMovieByGenre(genreId.toString())
            .enqueue(object : Callback<GenreMovieeeResponse> {
                override fun onResponse(
                    call: Call<GenreMovieeeResponse>, response: Response<GenreMovieeeResponse>
                ) {
                    val data = response.body()
                    println("cek data: $data")
                    result.value = if (response.isSuccessful && data != null) {
                        ApiResponse.Success(data)
                    } else {
                        ApiResponse.Error(response.message())
                    }
                }

                override fun onFailure(call: Call<GenreMovieeeResponse>, t: Throwable) {
                    val errorMessage = if (t is IOException) ERROR_MESSAGE_IO_EXCEPTION
                    else t.message.toString()
                    result.value = ApiResponse.Error(errorMessage)
                    Log.e(TAG, "${call.request().url}\n${t.stackTraceToString()}")
                }

            })

        return result
    }

    fun getMovieDetail(movieId: Int): LiveData<ApiResponse<MovieDetailResponse>> {
        val result = MutableLiveData<ApiResponse<MovieDetailResponse>>().apply {
            value = ApiResponse.Loading
        }

        apiService.getMovieDetail(movieId)
            .enqueue(object : Callback<MovieDetailResponse> {
                override fun onResponse(
                    call: Call<MovieDetailResponse>, response: Response<MovieDetailResponse>
                ) {
                    val data = response.body()
                    println("cek data: $data")
                    result.value = if (response.isSuccessful && data != null) {
                        ApiResponse.Success(data)
                    } else {
                        ApiResponse.Error(response.message())
                    }
                }

                override fun onFailure(call: Call<MovieDetailResponse>, t: Throwable) {
                    val errorMessage = if (t is IOException) ERROR_MESSAGE_IO_EXCEPTION
                    else t.message.toString()
                    result.value = ApiResponse.Error(errorMessage)
                    Log.e(TAG, "${call.request().url}\n${t.stackTraceToString()}")
                }

            })

        return result
    }
}