package putriiiiiuta.androidlima.movieee.data.repository

import androidx.lifecycle.LiveData
import putriiiiiuta.androidlima.movieee.data.remote.ApiResponse
import putriiiiiuta.androidlima.movieee.data.remote.MovieDetailResponse
import putriiiiiuta.androidlima.movieee.data.source.DataSource

class DetailMovieRepository private constructor(private val dataSource: DataSource) {

    companion object {
        @Volatile
        private var instance: DetailMovieRepository? = null

        fun getInstance(dataSource: DataSource): DetailMovieRepository {
            return instance ?: synchronized(this) {
                instance ?: DetailMovieRepository(dataSource)
            }
        }
    }

    fun getDetailMovie(movieId: Int): LiveData<ApiResponse<MovieDetailResponse>> {
        return dataSource.getMovieDetail(movieId)
    }

}