package putriiiiiuta.androidlima.movieeee.di

import android.content.Context
import putriiiiiuta.androidlima.movieee.network.ApiClient
import putriiiiiuta.androidlima.movieee.data.repository.DetailMovieRepository
import putriiiiiuta.androidlima.movieee.data.repository.GenreRepository
import putriiiiiuta.androidlima.movieee.data.repository.GenreMoveeeRepository
import putriiiiiuta.androidlima.movieee.data.source.DataSource

object databasemovie {

    fun provideGenreRepository(context: Context): GenreRepository {
        val apiService = ApiClient.provideApiService(context)
        val dataSource = DataSource.getInstance(apiService)

        return GenreRepository.getInstance(dataSource)
    }

    fun provideMovieByGenreRepository(context: Context): GenreMoveeeRepository {
        val apiService = ApiClient.provideApiService(context)
        val dataSource = DataSource.getInstance(apiService)

        return GenreMoveeeRepository.getInstance(dataSource)
    }

    fun provideDetailMovieRepository(context: Context): DetailMovieRepository {
        val apiService = ApiClient.provideApiService(context)
        val dataSource = DataSource.getInstance(apiService)

        return DetailMovieRepository.getInstance(dataSource)
    }
}