package putriiiiiuta.androidlima.movieee.common.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import putriiiiiuta.androidlima.movieee.data.repository.GenreRepository
import putriiiiiuta.androidlima.movieee.data.remote.ApiResponse
import putriiiiiuta.androidlima.movieee.data.remote.GenreResponse

class GenreViewModel(private val genreRepository: GenreRepository) : ViewModel() {

    fun getAllGenre(): LiveData<ApiResponse<GenreResponse>> {
        return genreRepository.getAllGenre()
    }
}