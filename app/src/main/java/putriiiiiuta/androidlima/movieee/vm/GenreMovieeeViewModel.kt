package putriiiiiuta.androidlima.movieee.common.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import putriiiiiuta.androidlima.movieee.data.repository.GenreMoveeeRepository
import putriiiiiuta.androidlima.movieee.data.remote.ApiResponse
import putriiiiiuta.androidlima.movieee.data.remote.GenreMovieeeResponse


class GenreMovieeeViewModel(private val genreMoveeeRepository: GenreMoveeeRepository) :
    ViewModel() {

    var genreId: Int? = null

    fun getMovieByGenre(): LiveData<ApiResponse<GenreMovieeeResponse>> {
        return genreMoveeeRepository.getMovieByGenre(genreId ?: 0)
    }
}