package putriiiiiuta.androidlima.movieee.vm

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import putriiiiiuta.androidlima.movieee.data.repository.DetailMovieRepository
import putriiiiiuta.androidlima.movieee.data.remote.ApiResponse
import putriiiiiuta.androidlima.movieee.data.remote.MovieDetailResponse

class DetailMovieeeViewModel(private val detailMovieRepository: DetailMovieRepository) : ViewModel() {

    var movieId: Int? = null

    fun getDetailMovie(): LiveData<ApiResponse<MovieDetailResponse>> {
        return detailMovieRepository.getDetailMovie(movieId ?: 0)
    }
}