package putriiiiiuta.androidlima.movieee.common.viewmodel

import android.content.Context
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import putriiiiiuta.androidlima.movieee.data.repository.DetailMovieRepository
import putriiiiiuta.androidlima.movieee.data.repository.GenreRepository
import putriiiiiuta.androidlima.movieee.data.repository.GenreMoveeeRepository
import putriiiiiuta.androidlima.movieee.vm.DetailMovieeeViewModel
import putriiiiiuta.androidlima.movieeee.di.databasemovie

class ViewModelFactory private constructor(
    private val genreRepository: GenreRepository,
    private val genreMoveeeRepository: GenreMoveeeRepository,
    private val detailMovieRepository: DetailMovieRepository
) : ViewModelProvider.NewInstanceFactory() {

    companion object {
        @Volatile
        private var instance: ViewModelFactory? = null

        fun getInstance(context: Context): ViewModelFactory {
            return instance ?: synchronized(this) {
                instance ?: ViewModelFactory(
                    databasemovie.provideGenreRepository(context),
                    databasemovie.provideMovieByGenreRepository(context),
                    databasemovie.provideDetailMovieRepository(context)
                )
            }
        }
    }

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel> create(modelClass: Class<T>): T =
        when {
            modelClass.isAssignableFrom(GenreViewModel::class.java) -> {
                GenreViewModel(genreRepository) as T
            }
            modelClass.isAssignableFrom(GenreMovieeeViewModel::class.java) -> {
                GenreMovieeeViewModel(genreMoveeeRepository) as T
            }
            modelClass.isAssignableFrom(DetailMovieeeViewModel::class.java) -> {
                DetailMovieeeViewModel(detailMovieRepository) as T
            }
            else -> throw Throwable("Unknown ViewModel class: ${modelClass.name}")
        }
}