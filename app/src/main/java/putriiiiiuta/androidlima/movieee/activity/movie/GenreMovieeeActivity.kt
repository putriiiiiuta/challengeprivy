package putriiiiiuta.androidlima.movieee.activity.movie

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.GridLayoutManager
import putriiiiiuta.androidlima.movieee.adapter.GenreMovieeeAdapter
import putriiiiiuta.androidlima.movieee.model.GenreModel
import putriiiiiuta.androidlima.movieee.model.MovieByGenreModel
import putriiiiiuta.androidlima.movieee.model.toMovieByGenreList
import putriiiiiuta.androidlima.movieee.data.remote.ApiResponse
import putriiiiiuta.androidlima.movieee.activity.detail.DetailMovieeeActivity
import putriiiiiuta.androidlima.movieee.common.viewmodel.GenreMovieeeViewModel
import putriiiiiuta.androidlima.movieee.common.viewmodel.ViewModelFactory
import putriiiiiuta.androidlima.movieee.databinding.ActivityGenreMovieeBinding

class GenreMovieeeActivity : AppCompatActivity() {
    private lateinit var binding: ActivityGenreMovieeBinding
    private lateinit var viewModel: GenreMovieeeViewModel
    private var genreMovieeeAdapter = GenreMovieeeAdapter()

    private lateinit var genreModel: GenreModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityGenreMovieeBinding.inflate(layoutInflater)
        setContentView(binding.root)


        initViewModel()
        getExtras()
        setupAdapter()
        populateData()
    }
    private fun getExtras() {
        intent.apply {
            genreModel = getParcelableExtra("EXTRA_GENRE")!!
            viewModel.genreId = genreModel.genreId
        }
        binding.apply {
            tvMovieByGenre.text = "Movie by ${genreModel.genreName}"
        }
    }

    private fun initViewModel() {
        val factory = ViewModelFactory.getInstance(this)
        viewModel = ViewModelProvider(this, factory)[GenreMovieeeViewModel::class.java]
    }

    private fun setupAdapter() {
        binding.rvMovieByGenre.apply {
            layoutManager = GridLayoutManager(this@GenreMovieeeActivity, 2)
            adapter = genreMovieeeAdapter

            genreMovieeeAdapter.setEventListener(object : GenreMovieeeAdapter.EventListener {
                override fun onItemClick(movie: MovieByGenreModel) {
                    val intent = Intent(
                        this@GenreMovieeeActivity, DetailMovieeeActivity::class.java
                    ).also { intent ->
                        intent.putExtra("EXTRA_MOVIE", movie)
                    }
                    startActivity(intent)
                }
            })
        }
    }

    private fun populateData() {
        viewModel.getMovieByGenre().observe(this) { result ->
            when (result) {
                is ApiResponse.Loading -> {
                    Toast.makeText(this, "loading", Toast.LENGTH_SHORT).show()
                }
                is ApiResponse.Success -> {
                    val movieList = result.data.toMovieByGenreList()
                    genreMovieeeAdapter.submitList(movieList)
                }
                is ApiResponse.Error -> {
                    Toast.makeText(this, result.errorMessage, Toast.LENGTH_SHORT)
                        .show()
                }
                else -> {}
            }
        }
    }

}