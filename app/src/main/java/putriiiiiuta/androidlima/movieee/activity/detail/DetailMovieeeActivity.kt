package putriiiiiuta.androidlima.movieee.activity.detail

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.lifecycle.ViewModelProvider
import com.bumptech.glide.Glide
import putriiiiiuta.androidlima.movieee.common.viewmodel.ViewModelFactory
import putriiiiiuta.androidlima.movieee.vm.DetailMovieeeViewModel
import putriiiiiuta.androidlima.movieee.model.MovieByGenreModel
import putriiiiiuta.androidlima.movieee.data.remote.ApiResponse
import putriiiiiuta.androidlima.movieee.databinding.ActivityDetailMovieeeBinding
import putriiiiiuta.androidlima.movieee.mimovie.data.model.toDetailMovie

class DetailMovieeeActivity : AppCompatActivity() {
    private lateinit var binding: ActivityDetailMovieeeBinding

    private lateinit var viewModel: DetailMovieeeViewModel
    private lateinit var movieModel: MovieByGenreModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityDetailMovieeeBinding.inflate(layoutInflater)
        setContentView(binding.root)

        initViewModel()
        getExtras()
        populateDataMovie()
    }
    private fun getExtras() {
        intent.apply {
            movieModel = getParcelableExtra("EXTRA_MOVIE")!!
            viewModel.movieId = movieModel.movieId
        }
    }

    private fun initViewModel() {
        val factory = ViewModelFactory.getInstance(this)
        viewModel = ViewModelProvider(this, factory)[DetailMovieeeViewModel::class.java]
    }

    private fun populateDataMovie() {
        viewModel.getDetailMovie().observe(this) { result ->
            when (result) {
                is ApiResponse.Loading -> {
                    Toast.makeText(this, "loading", Toast.LENGTH_SHORT).show()
                }
                is ApiResponse.Success -> {
                    val data = result.data.toDetailMovie()
                    binding.apply {
                        Glide.with(this@DetailMovieeeActivity)
                            .load("https://image.tmdb.org/t/p/original${data.movieImage}")
                            .into(ivMovieDetail)
                        tvMovieTitleDetail.text = data.movieTitle
                        tvMovieOverviewDetail.text = data.movieOverview
                    }
                }
                is ApiResponse.Error -> {
                    Toast.makeText(this, result.errorMessage, Toast.LENGTH_SHORT).show()
                }
                else -> {}
            }
        }
    }
}