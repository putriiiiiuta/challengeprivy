package putriiiiiuta.androidlima.movieee.activity.genre

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import putriiiiiuta.androidlima.movieee.common.viewmodel.GenreViewModel
import putriiiiiuta.androidlima.movieee.common.viewmodel.ViewModelFactory
import putriiiiiuta.androidlima.movieee.model.GenreModel
import putriiiiiuta.androidlima.movieee.model.toGenreList
import putriiiiiuta.androidlima.movieee.data.remote.ApiResponse
import putriiiiiuta.androidlima.movieee.databinding.ActivityMainBinding
import putriiiiiuta.androidlima.movieee.adapter.GenreAdapter
import putriiiiiuta.androidlima.movieee.activity.movie.GenreMovieeeActivity

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding

    private var genreAdapter = GenreAdapter()
    private lateinit var viewModel: GenreViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        initViewModel()
        setupAdapter()
        populateDataGenre()
    }

    private fun initViewModel() {
        val factory = ViewModelFactory.getInstance(this)
        viewModel = ViewModelProvider(this, factory)[GenreViewModel::class.java]
    }

    private fun setupAdapter() {
        binding.rvGenre.apply {
            layoutManager = LinearLayoutManager(this@MainActivity)
            adapter = genreAdapter

            genreAdapter.setEventListener(object : GenreAdapter.EventListener {
                override fun onItemClick(genre: GenreModel) {
                    val intent =
                        Intent(this@MainActivity, GenreMovieeeActivity::class.java).also { intent ->
                            intent.putExtra("EXTRA_GENRE", genre)
                        }
                    startActivity(intent)
                }
            })
        }
    }

    private fun populateDataGenre() {
        viewModel.getAllGenre().observe(this) { result ->
            when (result) {
                is ApiResponse.Loading -> {
                    Toast.makeText(this@MainActivity, "loading", Toast.LENGTH_SHORT).show()
                }
                is ApiResponse.Success -> {
                    val genreList = result.data.toGenreList()
                    genreAdapter.submitList(genreList)
                }
                is ApiResponse.Error -> {
                    Toast.makeText(this@MainActivity, result.errorMessage, Toast.LENGTH_SHORT)
                        .show()
                }
                else -> {}
            }
        }
    }
}
