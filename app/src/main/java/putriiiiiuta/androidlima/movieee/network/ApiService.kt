package putriiiiiuta.androidlima.movieee.network

import putriiiiiuta.androidlima.movieee.data.remote.GenreResponse
import putriiiiiuta.androidlima.movieee.data.remote.GenreMovieeeResponse
import putriiiiiuta.androidlima.movieee.data.remote.MovieDetailResponse
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface ApiService {

    @GET("genre/movie/list?api_key=${putriiiiiuta.androidlima.movieee.BuildConfig.MOVIE_API_KEY}")
    fun getAllGenre(): Call<GenreResponse>

    @GET("discover/movie?api_key=${putriiiiiuta.androidlima.movieee.BuildConfig.MOVIE_API_KEY}&page=1")
    fun getMovieByGenre(
        @Query("with_genres") withGenres: String
    ): Call<GenreMovieeeResponse>

    @GET("movie/{movie_id}?api_key=${putriiiiiuta.androidlima.movieee.BuildConfig.MOVIE_API_KEY}")
    fun getMovieDetail(
        @Path("movie_id") movieId: Int
    ): Call<MovieDetailResponse>
}